#pragma once

#include <string>

#include <ail/types.hpp>

namespace ail
{
	ulong time();
	ullong milliseconds();
	std::string timestamp();
	ullong boot_time();
}
